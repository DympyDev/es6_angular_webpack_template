# ES6, webpack and Angular 1.5.x template
This is a template to quickstart your development process. It is an Angular (1.5.x) application that makes use of ES6 and is bundled using webpack.
There are a number of different gulp tasks to ease the development process, as described in the underlying subparagraphs.

## Getting started
To get started with development, you first need to install the following:

- Node.js (latest is fine)
- Npm (latest is fine)
- An editor of your choice (if it has gulp integration and ES6 support, it's a big plus, I personally use Visual Studio Code or Sublime Text)

It is also recommended to have the following node packages installed globally:

- gulp (installed through `npm install gulp -g`)
- karma (installed through `npm install karma -g`)

When you have installed all of these, it is time to install the required packages as defined in the package.json.
This can be done by simply opening up a terminal, cd'ing into the project directory, and running the `npm install` task which will installed all required packages.
After this, you'll have to generate the application code using one of the gulp generate tasks, but you can read more about that under its own chapter.
After intializing, you can start the application by running one of the npm tasks, or one of the gulp tasks, from the project's location in the terminal (or through your IDE/editor if it supports it).

## NPM and Gulp build tasks
There are a number of tasks that can be run to help with the development process. There are build tasks, test tasks, watch tasks and generate tasks. 
There are also a number of npm tasks that act as aliases for the gulp tasks and other tasks.

### npm run build:dev
The `npm run build:dev` task triggers a script in the package.json that will use webpack to bundle the different javascript, HTML and CSS files, making them ready for distribution. All vendor related files are thrown into a vendor.bundle.js file and all app related files are thrown into the app.bundle.js file. These files are then copied over, as well as all files from the assets folder and an index.html file, to the dist folder. In the end, the contents of the dist folder is all you'll really need. THe difference between this task and the `npm run build:prod` task, is that this task does not uglify and minify the code.

### npm run build:prod
The `npm run build:prod` task triggers a script in the package.json that will use webpack to bundle the different javascript, HTML and CSS files, making them ready for distribution. All vendor related files are thrown into a vendor.bundle.js file and all app related files are thrown into the app.bundle.js file. These files are then copied over, as well as all files from the assets folder and an index.html file, to the dist folder. In the end, the contents of the dist folder is all you'll really need. THe difference between this task and the `npm run build:dev` task, is that this task also uglifies and minifies the code.

### npm run serve
The `npm run serve` task also uses webpack, but instead of creating a distribution ready folder, it will simply serve the source to a locally started HTTP server. This means you won't need to turn on any apache server or something. The started HTTP server also allows for hot reloading of the application, which means that every change you've saved will trigger a reload of the open instances of of the HTTP server, allowing you to immediately see the result of the changes you've made.

### npm run clean
The `gulp clean` task removes the dist folder (when present) to start anew when creating a new release. This task is called prior to the bundling of the application when calling the `npm run build:dev` or `npm run build:prod` tasks.

### npm run test
The `npm run test` task runs the `karma start` task, which kickstarts the unit tests.

## Gulp generator tasks
To mainstream the generation of components and routes, I have created a few templates that will generate a new component or route-component based on a few arguments passed to the task itself.

### Initializing the application
As you'll most likely have noticed, the source folder is empty by default. This folder can be populated using the `gulp generate:init-app` task.
This task will copy over the basic setup for the application, it will not create a default route component, so you still have to generate that.
There are no argument, however it is recommended to change the `APP_NAME` constant in the generator task file (located at `tasks/generator.js`), this will reflect through all the other generate tasks as well.
If you're fine with your app being name "app", then you won't have to change a thing.

### Generating common components
If you want to generate a common component, mostly used for generic UI elements, you can use the `gulp generate:common-component` task.
The task takes a number of different arguments, some required, some optional:
#### --name
The name for this component, in kebab-case as it is transformed into a CamelCased variant for the naming of the controllers and inner variables. Should be provided as `--name <name>`.
#### --prefix
An optional prefix for the component registration and for the folder name. It is added before the provided name argument and either kebab-cased or CamelCased, depending on the situation. Should be provided as `--prefix <prefix>`.
#### --parent
An optional parent path if you'd want the generated component to be located outside of the default folder (/src/app/common/). Should be provided as `--parent <parent path>`.

### Generating route components
If you want to generate a route component, used for the different routes/pages your application will contain, you can use the `gulp generate:route-component` task.
The task takes a number of different arguments, some required, some optional:
#### --name
The name for this component, in kebab-case as it is transformed into a CamelCased variant for the naming of the controllers and inner variables. Should be provided as `--name <name>`.
#### --route
The route that should be used for this component, can be either CamelCase or kebab-case. Should be provided as `--route <route>`.
#### --prefix
An optional prefix for the component registration and for the folder name. It is added before the provided name argument and either kebab-cased or CamelCased, depending on the situation. Should be provided as `--prefix <prefix>`.
#### --parent
An optional parent path if you'd want the generated component to be located outside of the default folder (/src/app/common/). Should be provided as `--parent <parent path>`.

## TODO; The future updates I plan to add some day
- Remove gulp altogether. It is only used for generating components, but I think this can be done a bit easier/user-friendly
- Update Angular to 1.6.x. In the current templating code, there still happens some stuff in the constructor, which is a big no-no in Angular 1.6.x
- Angular2 version of this template/generator project. Maybe with Typescript, maybe with ES6, maybe options for both?
- Move this from a boilerplate template code to a full-fletched project setup npm script (so when installed globally, you'll have access to it in any folder from the terminal).