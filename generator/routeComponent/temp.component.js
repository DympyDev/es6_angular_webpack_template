import template from './<%= fileName %>.html';
import controller from './<%= fileName %>.controller';
import './<%= fileName %>.scss';

let <%= jsComponentName %>Component = {
	restrict: 'E',
	bindings: {},
	template,
	controller
};

export default <%= jsComponentName %>Component;
