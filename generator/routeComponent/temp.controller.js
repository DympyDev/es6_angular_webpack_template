class <%= controllerName %>Controller {
	constructor() {
		this.name = '<%= ngComponentName %>';
	}
}

export default <%= controllerName %>Controller;
