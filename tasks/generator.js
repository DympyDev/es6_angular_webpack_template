import gulp from 'gulp';
import path from 'path';
import rename from 'gulp-rename';
import template from 'gulp-template';
import yargs from 'yargs';

// TODO: Change me if you want to have a different root app name
const APP_NAME = 'test-app';

const SRC_PATH = '../src';

// helper method for resolving paths
let resolveToCommonComponents = (glob = '') => {
	return path.join(SRC_PATH, 'app/common', glob); // src/app/common/{glob}
};

let resolveToRouteComponents = (glob = '') => {
	return path.join(SRC_PATH, 'app/components', glob); // src/app/components/{glob}
};

const capitalized = (val) => {
	return val.charAt(0).toUpperCase() + val.slice(1);
};

const replaceHyphens = (val) => {
    return val.replace(/(\-\w)/g, (matchedResult) => matchedResult[1].toUpperCase());
};

// map of all paths
let paths = {
	appTemplatePath: path.join(__dirname, '../generator', 'initApp/**/*.**'),
	commonTemplatePath: path.join(__dirname, '../generator', 'commonComponent/**/*.**'),
	routeTemplatePath: path.join(__dirname, '../generator', 'routeComponent/**/*.**'),
	appDestPath: () => path.join(__dirname, SRC_PATH),
	commonDestPath: (glob = '') => path.join(__dirname, SRC_PATH, 'app/common', glob),
	routeDestPath: (glob = '') => path.join(__dirname, SRC_PATH, 'app/components', glob)
};

gulp.task('generate:init-app', () => {
	return gulp.src(paths.appTemplatePath)
		.pipe(template({
			appName: APP_NAME
		}))
		.pipe(gulp.dest(paths.appDestPath));
});

gulp.task('generate:common-component', () => {
	if (yargs.argv.help || yargs.argv.h) {
		console.log('\nArguments:\n--name\t\tThe name for the new common component, in kebab-case\n--prefix\tWhat the files and component should be prefixed with\n--parent\tThe parent path, in case you want to locate this component elsewhere\n');	
		return;
	}
	if (!yargs.argv.name) {
		console.log('\nThis task requires the "name" parameter.\nRun this task with the "--help" or "--h" flag to see what this argument does and what other arguments are available.\n');
		return;
	}

	const prefix = yargs.argv.prefix || '';
	const parentPath = yargs.argv.parent || '';

	const fileName = yargs.argv.name;
	const folderName = (prefix.length > 0 ? prefix + '-' : '') + fileName;
	const jsComponentName = replaceHyphens(fileName);
	const ngComponentName = replaceHyphens(folderName);
	const controllerName = capitalized(replaceHyphens(fileName));
	const destPath = path.join(paths.commonDestPath(), parentPath, folderName);

	return copyTemplate(paths.commonTemplatePath, {
			appName: APP_NAME,
			fileName: fileName,
			jsComponentName: jsComponentName,
			ngComponentName: ngComponentName,
			controllerName: controllerName
		}, fileName, destPath);
});

gulp.task('generate:route-component', () => {
	if (yargs.argv.help || yargs.argv.h) {
		console.log('\nArguments:\n--name\t\tThe name for the new route component, in kebab-case\n--route\t\tThe prefered route for this component, if empty, defaults to /.\n--prefix\tWhat the files and component should be prefixed with\n--parent\tThe parent path, in case you want to locate this component elsewhere\n');	
		return;
	}
	if (!yargs.argv.name) {
		console.log('\nThis task requires the "name" and "route" parameter. If the route parameter is not passed, it will default to /.\nRun this task with the "--help" or "--h" flag to see what these arguments do and what other arguments are available.\n');
		return;
	}

	const prefix = yargs.argv.prefix || '';
	const parentPath = yargs.argv.parent || '';

	const fileName = yargs.argv.name;
	const routeName = yargs.argv.route || '';
	const folderName = (prefix.length > 0 ? prefix + '-' : '') + fileName;
	const jsComponentName = replaceHyphens(fileName);
	const ngComponentName = replaceHyphens(folderName);
	const controllerName = capitalized(replaceHyphens(fileName));
	const destPath = path.join(paths.routeDestPath(), parentPath, folderName);

	return copyTemplate(paths.routeTemplatePath, {
			appName: APP_NAME,
			fileName: fileName,
			routeName: routeName,
			jsComponentName: jsComponentName,
			ngComponentName: ngComponentName,
			controllerName: controllerName
		}, fileName, destPath);
});

function copyTemplate(templatePath, templateOptions, fileName, destinationPath) {
	return gulp.src(templatePath)
		.pipe(template(templateOptions))
		.pipe(rename((path) => {
			path.basename = path.basename.replace('temp', fileName);
		}))
		.pipe(gulp.dest(destinationPath));
}