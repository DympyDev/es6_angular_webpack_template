import angular from 'angular';

// TODO: Import the generated common component modules here

let commonModule = angular.module('<%= appName %>.common', [
		// TODO: Add the imported models here
	]);

export default commonModule.name;
