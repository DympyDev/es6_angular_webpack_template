import angular from 'angular';
import uiRouter from 'angular-ui-router';

import <%= jsComponentName %>Component from './<%= fileName %>.component';
import <%= jsComponentName %>Service from './<%= fileName %>.service';

let <%= jsComponentName %>Module = angular.module('<%= appName %>.components.<%= ngComponentName %>', [
		uiRouter
	])
	.config(($stateProvider, $urlRouterProvider) => {
		"ngInject";

		$stateProvider
			.state('<%= ngComponentName %>', {
				url: '/<%= routeName %>',
				component: '<%= ngComponentName %>'
			});
	})
	.service('<%= ngComponentName %>Service', <%= jsComponentName %>Service)
	.component('<%= ngComponentName %>', <%= jsComponentName %>Component);

export default <%= jsComponentName %>Module.name;
