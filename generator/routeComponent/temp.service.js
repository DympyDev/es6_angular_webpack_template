class <%= controllerName %>Service {
	/*@ngInject*/
	constructor($http) {
		this._$http = $http;
	}
}

export default <%= controllerName %>Service;