import angular from 'angular';

// TODO: Import the generated route component modules here

let componentModule = angular.module('<%= appName %>.components', [
		// TODO: Add the imported models here
	]);

export default componentModule.name;
