var path = require('path');
var webpack = require('webpack');
var ngAnnotatePlugin = require('ng-annotate-webpack-plugin')
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');

let config = {
	devtool: 'source-map',
	entry: __dirname + '/src/app/app.js',
	output: {
		filename: '[name].bundle.js',
		path: path.resolve(__dirname, 'dist')
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: [
					/app\/lib/,
					/node_modules/
				],
				use: [
					{
						loader: 'babel-loader',
						options: {
							babelrc: false,
							presets: [
								["env", {
									"targets": {
										"browsers": [
											"last 2 versions",
											"safari >= 7"
										]
									},
									modules: false
								}]
							]
						}
					}
				]
			},
			{
				test: /\.html$/,
				use: ['html-loader']
			},
			{
				test: /\.scss$/,
				use: [
					'style-loader',
					'css-loader',
					'sass-loader'
				]
			},
			{
				test: /\.css$/,
				use: [
					'style-loader',
					'css-loader'
				]
			},
			{
				test: /\.(png|jpeg|jpg|gif)$/,
				use: ["url-loader?limit=5000&name=img/img-[hash:6].[ext]"]
			},
			{
				test: /\.woff$/,
				use: ['file-loader']
			}
		]
	},
	plugins: [
		new ngAnnotatePlugin({
            add: true,
            // other ng-annotate options here 
        }),
		new HtmlWebpackPlugin({
			template: 'src/index.html',
			inject: 'body',
			hash: true
		}),
		new webpack.optimize.CommonsChunkPlugin({
			name: 'vendor',
			minChunks: function (module, count) {
				return module.resource && module.resource.indexOf(path.resolve(__dirname, 'src')) === -1;
			}
		}),
		new CopyWebpackPlugin([
            {
            	from: 'src/assets',
            	to: 'assets'
            }
        ])
	]
};


module.exports = (env = {}) => {
	if (env.production === true ) {
		config.plugins = config.plugins.concat([
			new webpack.optimize.UglifyJsPlugin({
				mangle: {
					except: ['$super', '$', 'exports', 'require', 'angular']
				}
			})
		]);
	} else {
		config.plugins = config.plugins.concat([
			new webpack.HotModuleReplacementPlugin()
		]);
	}

	return config;
};
