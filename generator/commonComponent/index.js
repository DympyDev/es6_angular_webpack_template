import angular from 'angular';
import <%= jsComponentName %>Component from './<%= fileName %>.component';

let <%= jsComponentName %>Module = angular.module('<%= appName %>.common.<%= ngComponentName %>', [])
	.component('<%= ngComponentName %>', <%= jsComponentName %>Component);

export default <%= jsComponentName %>Module.name;
