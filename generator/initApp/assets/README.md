Add assets, such as fonts, in this folder.
These assets will not be included in your source bundle (unless you import them in your code), but will be copied to your dist folder.
This is handy if you need to put certain licenses on your site, or if you want to keep a log of all your used dependencies.