import angular from 'angular';
import ngSanitize from 'angular-sanitize';
import uiRouter from 'angular-ui-router';
import 'normalize.css';

import Common from './common';
import Components from './components';
import AppComponent from './app.component';

angular.module('<%= appName %>', [
		ngSanitize,
		uiRouter,
		Common,
		Components
	])
	.config(($urlRouterProvider) => {
		"ngInject";

		$urlRouterProvider.otherwise('/');
	})
	.component('app', AppComponent);
